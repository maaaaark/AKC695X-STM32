/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "AKC695X.h"
#include <stdio.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#define REC_LENGTH  1
#define MAX_REC_LENGTH  1024 
  
unsigned char UART1_Rx_Buf[MAX_REC_LENGTH];
unsigned char UART1_Rx_flg ;
unsigned int  UART1_Rx_cnt ;
unsigned char UART1_temp[REC_LENGTH];
	AKC695X radio;
	uint16_t currentFM = 895;
	uint16_t currentAM = 810;
	uint16_t currentCB = 760;
	uint16_t currentFrequency;
	uint8_t flag = 1;
	uint8_t I;
	uint8_t bass=0;
	uint8_t RxCounter1=0,RxBuffer1[50]={0},RxTemp1=0,F_Usart1=0;
	void showStatus();
	void showhelp();
	
	
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
	HAL_UART_Receive_IT(&huart1,(uint8_t *)UART1_temp,REC_LENGTH);
	reset();
	HAL_Delay(1000);
	radio.setup(0,0x20);
	HAL_UART_Transmit(&huart1,(uint8_t *)"OPEN FM...\r\n",sizeof("OPEN FM...\r\n"),10);
	HAL_Delay(1000);
	currentFrequency = currentFM;
	radio.setAudio();
	//HAL_Delay(1000);
	radio.setFM(0, 870, 1080, currentFrequency, 1);
	HAL_Delay(500);

	showhelp();
  showStatus();

	
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    if(UART1_Rx_flg)
    {
      HAL_UART_Transmit(&huart1,UART1_temp,sizeof(UART1_temp),0x10); 
			char ch;
			ch =UART1_temp[0];
			
			switch (ch){
			case '+':
        radio.setVolumeUp();
        break;
      case '-':
        radio.setVolumeDown();
        break;
      case 'a':
      case 'A':
        if (radio.getCurrentMode() == 1)
        {
          if (flag == 1) {
            currentFM = currentFrequency;
            radio.setAM(3, 550, 1710, currentAM, 5);
          }
          else {
            currentCB = currentFrequency;
            radio.setAM(3, 550, 1710, currentAM, 5);
          }
        }
        else
        {
          radio.setAM(3, 550, 1710, currentFrequency, 5);
        }
        break;
      case 'f':
      case 'F':
        if (radio.getCurrentMode() == 0)
        {
          currentAM = currentFrequency;
          radio.setFM(0, 870, 1080, currentFM, 1);
        }
        else if (flag == 0)
        {
          currentCB = currentFrequency;
          radio.setFM(0, 870, 1080, currentFM, 1);
        }
        else
        {
          radio.setFM(0, 870, 1080, currentFrequency, 1);
        }
        flag = 1;
        break;
      case 'c':
      case 'C': //open Campus Broadcasting
        if (radio.getCurrentMode() == 0)
        {
          currentAM = currentFrequency;
          radio.setFM(1, 760, 1080, currentCB, 1);
        }
        else if (flag == 1) {
          currentFM = currentFrequency;
          radio.setFM(1, 760, 1080, currentCB, 1);
        }
        else
        {
          radio.setFM(1, 760, 1080, currentFrequency, 1);
        }
        flag = 0;
        break;
      case '1':
        radio.setAM(10, 11400, 12200, 11940, 5);
        break;
      case 'U':
      case 'u':
        radio.frequencyUp();
        break;
      case 'D':
      case 'd':
        radio.frequencyDown();
        break;
      case 'S':
        radio.seekStation(1);
        break;
      case 's':
        radio.seekStation(0);
        break;
      case 'b':
      case 'B':
        if (bass==0)
        {radio.setbass(1); bass=1;}
        else
        {radio.setbass(0); bass=0;}
        break;
      case '0':
        showStatus();
        break;
      case '?':
        showhelp();
        break;
			case 'r':
			case 'R':
				HAL_UART_Receive_IT(&huart1,(uint8_t *)UART1_temp,REC_LENGTH);
				reset();
				radio.setup(0,0x20);
				HAL_UART_Transmit(&huart1,(uint8_t *)"OPEN FM...\r\n",sizeof("OPEN FM...\r\n"),10);
				HAL_Delay(1000);
				currentFrequency = currentFM;
			  radio.setAudio();
				radio.setFM(0, 870, 1080, currentFrequency, 1);
				
				showhelp();
				showStatus();
				break;
      default:
        //showStatus();
        break;
		}

			showStatus();
      UART1_Rx_cnt = 0;
      UART1_Rx_flg = 0;
    }
		
		HAL_Delay(200);

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 50000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 64;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */
  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);

  /*Configure GPIO pin : PB5 */
  GPIO_InitStruct.Pin = GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if(huart->Instance==USART1)
  {
    UART1_Rx_flg = 1;
    HAL_UART_Receive_IT(&huart1,(uint8_t *)UART1_temp,REC_LENGTH);
  }
}

uint8_t AKC695X::getRegister(uint8_t reg){
	uint8_t result[1],data[1];
	data[0]=reg;
	HAL_I2C_Master_Transmit(&hi2c1,this->deviceAddress,data,1,100);
	HAL_I2C_Master_Receive(&hi2c1,this->deviceAddress,result,1,100);
	return result[0];
}
void AKC695X::setRegister(uint8_t reg, uint8_t parameter){
	uint8_t data[2];
	data[0]=reg;
	data[1]=parameter;
	HAL_I2C_Master_Transmit(&hi2c1,this->deviceAddress,data,2,100);
}

void showhelp(){
	HAL_UART_Transmit(&huart1,(uint8_t *)"\nType F/f to FM; A/a to MW; 1 to SW",sizeof("\nType F/f to FM; A/a to MW; 1 to SW"),20);
	HAL_UART_Transmit(&huart1,(uint8_t *)"\nType C/c to open Campus Broadcasting",sizeof("\nType C/c to open Campus Broadcasting"),20);
	HAL_UART_Transmit(&huart1,(uint8_t *)"\nType U to increase and D to decrease the frequency",sizeof("\nType U to increase and D to decrease the frequency"),20);
	HAL_UART_Transmit(&huart1,(uint8_t *)"\nType S or s to seek station Up or Down",sizeof("\nType S or s to seek station Up or Down"),20);
	HAL_UART_Transmit(&huart1,(uint8_t *)"\nType R/r to reset the dsp to the default mode",sizeof("\nType 0 to show current status,? to this help."),20);
	HAL_UART_Transmit(&huart1,(uint8_t *)"\nType 0 to show current status,? to this help.",sizeof("\nType 0 to show current status,? to this help."),20);
	HAL_UART_Transmit(&huart1,(uint8_t *)"\n==================================================\n",sizeof("\n==================================================\n"),20);
}
void showStatus()
{
	
float freq,voltage;
	int RSSI,vol;
	currentFrequency = radio.getFrequency();
  freq= (radio.getCurrentMode() == CURRENT_MODE_FM) ? currentFrequency / 10.0 : currentFrequency / 1000.0;
	voltage=radio.getSupplyVoltage();
	RSSI=radio.getRSSI();
	vol = radio.getVolume();
	char Message[100];
	uint8_t MessageLen = sprintf((char *)Message, "\nYou are tuned on%.3fMHZ", freq);
  HAL_UART_Transmit(&huart1,(uint8_t *) Message, MessageLen, 100);
	MessageLen = sprintf((char *)Message, "  RSSI:%d", RSSI);
  HAL_UART_Transmit(&huart1,(uint8_t *) Message, MessageLen, 100);
	MessageLen = sprintf((char *)Message, "  Battery:%.3fV", voltage);
  HAL_UART_Transmit(&huart1,(uint8_t *) Message, MessageLen, 100);
	MessageLen = sprintf((char *)Message, "  Volume:%d", vol);
  HAL_UART_Transmit(&huart1,(uint8_t *) Message, MessageLen, 100);
}

void reset(){
HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,GPIO_PIN_RESET);
HAL_Delay(50);
HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,GPIO_PIN_SET);
HAL_Delay(50);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
