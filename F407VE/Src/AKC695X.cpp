#include "AKC695X.h"
#include "main.h"
#include "stm32f4xx_hal.h"
/**
 * SI47XX Arduino Library implementation 
 * 
 * This is an Arduino library for the AKC695X, BROADCAST RECEIVER, IC family.  
 * It works with I2C protocol and can provide an easier interface for controlling the AKC695X devices.<br>
 * 
 * This library was built based on [AKC6955 stereo FM / TV / MW / SW / LW digital tuning radio](http://maximradio.altervista.org/akc6955/AKC6955-datasheet-english.pdf) document from "AKC technology". 
 * It also intend to  be used on **all members of the AKC695X family** respecting, of course, the features available for each IC version. 
 * 
 * This library can be freely distributed using the MIT Free Software model. [Copyright (c) 2019 Ricardo Lima Caratti](https://pu2clr.github.io/AKC695X/#mit-license).  
 * Contact: pu2clr@gmail.com
 */
 void AKC695X::setI2CBusAddress(uint8_t ADD){
	this->deviceAddress=ADD;
}
void AKC695X::setup(uint8_t crystal_type,uint8_t ADD){
	setI2CBusAddress(ADD);
	setCrystalType(crystal_type);
	this->currentMode = 0;
	this->currentMode3k = 1;
	this->currentBand = 0;
	this->fmCurrentBand = 0;
}
void AKC695X::setCrystalType(uint8_t crystal){
	akc595x_reg2 reg2; 
	reg2.raw=getRegister(REG02);
	reg2.refined.ref_32k_mode = crystal;
	setRegister(REG02,reg2.raw);
	this->currentCrystalType = crystal;
}


void AKC695X::powerOn(uint8_t fm_en, uint8_t tune, uint8_t mute, uint8_t seek, uint8_t seekup){
		akc595x_reg0 reg0;
    reg0.refined.power_on = 1;
    reg0.refined.rsv = 0;
    reg0.refined.fm_en = fm_en;
    reg0.refined.mute = mute;
    reg0.refined.seek = seek;
    reg0.refined.seekup = seekup;
    reg0.refined.tune = tune;
    setRegister(REG00, reg0.raw);
    this->currentMode = fm_en;
}

bool AKC695X::isTuned() {
    akc595x_reg20 reg20;
    reg20.raw = getRegister(REG20);
    return reg20.refined.tuned;
}
bool AKC695X::isTuningComplete(){
    akc595x_reg20 reg20;
    reg20.raw = getRegister(REG20);
    return reg20.refined.stc;
}

uint8_t AKC695X::isCurrentModeFM(){
    akc595x_reg20 reg20;
    reg20.raw = getRegister(REG20);
    return reg20.refined.st;
}
uint16_t AKC695X::getCurrentChannel() {

    akc595x_reg20 reg20;
    akc595x_reg21 reg21;
    uint16_t channel = 0;

    reg20.raw = getRegister(REG20);
    reg21 = getRegister(REG21);

    channel = reg20.refined.readchan;
    channel = channel << 8;
    channel = channel | reg21;

    return channel;
}
uint16_t AKC695X::channelToFrequency(){
    uint16_t frequency;
    if (this->currentMode == CURRENT_MODE_FM)
    {
        // the FM tuned frequency = channel / 4 + 300.
        frequency = (getCurrentChannel() >> 2) + 300;
    }
    else
    {
        // the AM tuned frequency is channel * current step.
        frequency = getCurrentChannel() * this->currentStep;
    }
    return frequency;
}
uint8_t AKC695X::getAmCarrierNoiseRatio(){
    akc595x_reg22 reg22;
    reg22.raw = getRegister(REG22);
    return reg22.refined.cnram;
}
uint8_t AKC695X::getAmCurrentSpace(){
    akc595x_reg22 reg22;
    reg22.raw = getRegister(REG22);
    return reg22.refined.mode3k_f;
}
bool AKC695X::isFmStereo() {
    akc595x_reg23 reg23;
    reg23.raw = getRegister(REG23);
    return reg23.refined.st_dem;
}
uint8_t AKC695X::getFmCarrierNoiseRatio(){
    akc595x_reg23 reg23;
    reg23.raw = getRegister(REG23);
    return reg23.refined.cnrfm;
}


void AKC695X::setFmEmphasis( uint8_t de) {
    akc595x_reg7 reg7;
    reg7.raw = getRegister(REG07); // Gets the current value 
    reg7.refined.de = de;          // Sets just DE attribute 
    setRegister(REG07, reg7.raw);        // Store the new REG07 content 
}
void AKC695X::setFmStereoMono(uint8_t value){
    akc595x_reg7 reg7;
    reg7.raw = getRegister(REG07);      // Gets the current value
    reg7.refined.stereo_mono = value;   // Sets just the attribute
    setRegister(REG07, reg7.raw);       // Store the new REG07 content
}
/**
 * @ingroup GA03A
 * @brief Sets the FM Bandwidth  
 * @details This method configures FM Bandwidth  
 * 
 * | Parameter vaue |  Bandwidth |
 * | -------------- | -----------| 
 * |       0        | 150KHz     | 
 * |       1        | 200KHz     | 
 * |       2        | 50KHz      | 
 * |       3        | 100KHz     |  
 * 
 * @param value     see table above
 */
void AKC695X::setFmBandwidth(uint8_t value){
    akc595x_reg7 reg7;
    reg7.raw = getRegister(REG07); // Gets the current value
    reg7.refined.bw = value;      // Sets just the attribute
    setRegister(REG07, reg7.raw);   // Store the new REG07 content
}

void AKC695X::commitTune(){
    akc595x_reg0 reg0;
    reg0.raw = 0;
    reg0.refined.fm_en = this->currentMode; // Sets to the current mode
    reg0.refined.power_on = 1;
    reg0.refined.tune = 1;
    setRegister(REG00, reg0.raw);
    reg0.refined.tune = 0;
    reg0.refined.seek = 0;
    reg0.refined.seekup = 0;
    setRegister(REG00, reg0.raw);
}
void AKC695X::setCustomBand(uint16_t minimum_frequency, uint16_t maximum_frequency) {

    uint8_t start_channel, end_channel;
    akc595x_reg4 reg4; // start channel for custom band
    akc595x_reg5 reg5; // end channel for custom band

    if (this->currentMode == CURRENT_MODE_FM)
    {
        start_channel = (minimum_frequency - 300) * 4;
        end_channel = (maximum_frequency - 300) * 4;
    }
    else
    {
        start_channel = minimum_frequency / this->currentStep;
        end_channel = maximum_frequency / this->currentStep;
    }

    reg4 = start_channel / 32;
    reg5 = end_channel / 32;

    setRegister(REG04, reg4);
    setRegister(REG05, reg5);
}

/**
 * @ingroup GA04
 * @brief Sets the AKC695X to FM mode
 * @details Sets the device to FM mode. You can configure a custom FM band by setting band number greater than 7.
 * 
 * | FM band | N#  |Description  |
 * | --------| --- |------------ |
 * | 000     |  0  | FM1,87 ~ 108, station search space specified intervals |
 * | 001     |  1  | FM2,76 ~ 108, station search space specified intervals |
 * | 010     |  2  | FM3,70 ~ 93, with a space station search interval set |
 * | 011     |  3  | FM4,76 ~ 90, Tuning predetermined space intervals |
 * | 100     |  4  | FM5,64 ~ 88, with a space station search interval set | 
 * | 101     |  5  | TV1,56.25 ~ 91.75, station search space specified intervals |
 * | 110     |  6  | TV2, 174.75 ~ 222.25, found |
 * | 111     |  7  | sets predetermined space intervals, custom FM, station search space specified intervals |
 * 
 * @param akc695x_fm_band       FM band (see manual FM band table above). Set to a number greater than 7 if you want a custom FM band.
 * @param minimum_freq          Minimal frequency of the band 
 * @param maximum_freq          Band maximum frequency
 * @param default_frequency     default frequency
 * @param default_step          increment and decrement step 
 */
void AKC695X::setFM(uint8_t akc695x_fm_band, uint16_t minimum_freq, uint16_t maximum_freq, uint16_t default_frequency, uint8_t default_step){
    uint16_t channel;
    uint8_t high_bit, low_bit;

    akc595x_reg1 reg1;
    akc595x_reg2 reg2;

    this->currentMode = 1;
    this->currentBand = akc695x_fm_band;
    this->currentBandMinimumFrequency = minimum_freq;
    this->currentBandMaximumFrequency = maximum_freq;
    this->currentFrequency = default_frequency;
    this->currentStep = default_step;

    reg1.raw = 0;
    reg1.refined.fmband = akc695x_fm_band; // Selects the band will be used for FM (see fm band table)
    setRegister(REG00,0xC8);  //0b11001000
    if (akc695x_fm_band > 6 )
        setCustomBand(minimum_freq, maximum_freq); // Sets a custom FM band 

    setRegister(REG01, reg1.raw); // Sets the FM band

    channel = (default_frequency - 300) * 4;
    high_bit = (channel >> 8); 
    low_bit = channel & 0x0ff;

    setRegister(REG03, low_bit);

    reg2.raw = high_bit;
    reg2.refined.ref_32k_mode = this->currentCrystalType;
    reg2.refined.mode3k = this->currentMode3k;

    setRegister(REG02, reg2.raw); // Needs consider the crystal type

    commitTune();
}

/**
 * @ingroup GA04
 * @brief Sets the AKC695X to AM mode and selects the band
 * @details This method configures the AM band you want to use. 
 * @details You must respect the frequency limits defined by the AKC595X device documentation.
 * @details You can configure a custom band by setting a band greater than 17 
 * 
 * | AM band      | N#  |Description  |
 * | ------------ | --- |------------ |
 * | 00000        |  0  |LW, 0.15 ~ 0.285, 3K station search |
 * | 00001        |  1  |MW1, 0.52 ~ 1.71, 5K station search |
 * | 00010        |  2  |MW2, 0.522 ~ 1.62, 9K station search |
 * | 00011        |  3  |MW3, 0.52 ~ 1.71, 10K station search |
 * | 00100        |  4  |SW1, 4.7 ~ 10, 5K station search |
 * | 00101        |  5  |SW2, 3.2 ~ 4.1, 5K station search |
 * | 00110        |  6  |SW3, 4.7 ~ 5.6, 5K station search |
 * | 00111        |  7  |SW4, 5.7 ~ 6.4, 5K station search |
 * | 01000        |  8  |SW5, 6.8 ~ 7.6, 5K station search |
 * | 01001        |  9  |SW6, 9.2 ~ 10, 5K station search |
 * | 01010        | 10  |SW7, 11.4 ~ 12.2, 5K station search |
 * | 01011        | 11  |SW8, 13.5 ~ 14.3 |
 * | 01100        | 12  |SW9, 15 ~ 15.9 |
 * | 01101        | 13  |SW10, 17.4 ~ 17.9 |
 * | 01110        | 14  |SW11, 18.9 ~ 19.7, 5K station search |
 * | 01111        | 15  |SW12, 21.4 ~ 21.9, 5K station search |
 * | 10000        | 16  |SW13, 11.4 ~ 17.9, 5K station search |
 * | 10010        | 17  |MW4, 0.52 to 1.73, 5K station search |
 * | Other        | 18+ |custom band, station search interval = 3K |
 * 
 * @param akc695x_am_band       AM band. Set to a value greater than 17 if you want a custom AM band (see manual AM band table above)
 * @param minimum_freq          Minimal frequency of the band 
 * @param maximum_freq          Band maximum frequency
 * @param default_frequency     default frequency
 * @param default_step          increment and decrement step 
 */
void AKC695X::setAM(uint8_t akc695x_am_band, uint16_t minimum_freq, uint16_t maximum_freq, uint16_t default_frequency, uint8_t default_step){
    uint16_t channel;
    uint8_t high_bit, low_bit;

    akc595x_reg1 reg1;
    akc595x_reg2 reg2;

    this->currentMode = 0;
    this->currentBand = akc695x_am_band;
    this->currentBandMinimumFrequency = minimum_freq;
    this->currentBandMaximumFrequency = maximum_freq;
    this->currentFrequency = default_frequency;
    this->currentStep = default_step;

    reg1.raw = 0;
    reg1.refined.amband = akc695x_am_band; // Selects the AM band will be used (see AM band table)

    //setRegister(REG00, 0b10000000); // Sets to AM (Power On)
    setRegister(REG00, 0x88); 
    if (akc695x_am_band > 17)
        setCustomBand(minimum_freq, maximum_freq); // Sets a custom AM band

    setRegister(REG01, reg1.raw); 

    channel = default_frequency / this->currentStep;
    high_bit = (channel >> 8); 
    low_bit = channel & 0x0ff;

    setRegister(REG03, low_bit);

    reg2.raw = high_bit;
    reg2.refined.ref_32k_mode = this->currentCrystalType;
    reg2.refined.mode3k = this->currentMode3k;

    setRegister(REG02, reg2.raw); 

    commitTune();
}

void AKC695X::setStep(uint8_t step){
    this->currentStep = step;
}
/**
 * @ingroup GA04
 * @brief Sets FM step for seeking. 
 * 
 * @details Sets FM seek step. 
 * 
 * | spece | N#  | step    |
 * | ----- | --- | ------- | 
 * |  00   |  0  | 25 KHz  |
 * |  01   |  1  | 50 KHz  | 
 * |  10   |  2  | 100 KHz | 
 * |  11   |  3  | 200 KHz | 
 * 
 * @see AKC6955 stereo FM / TV / MW / SW / LW digital tuning radio documentation; page 14
 * 
 * @param space value betwenn 0 and 3 (see table above). 
 */
void AKC695X::setFmSeekStep(uint8_t space){
    akc595x_reg11 reg11;
    reg11.refined.space = (space > 3) ? 3 : space;
    setRegister(REG11, reg11.raw);
}
void AKC695X::seekStation(uint8_t up_down, void (*showFunc)()) {
		//time�߼�����һ��
    akc595x_reg0 reg0;
    //long max_time = 0;
    do {
        reg0.raw = 0;
        reg0.refined.fm_en = this->currentMode;     // Sets the current mode
        reg0.refined.mute = 0;                      // Normal operation
        reg0.refined.power_on = 1;                  // Power on
        reg0.refined.tune = 0;      
        reg0.refined.seek = 1;                      // Trigger seeking process 
        reg0.refined.seekup = up_down;
        setRegister(REG00, reg0.raw);
        if (showFunc != NULL) {
            this->currentFrequency = channelToFrequency(); // gets the Current frequency in the registers 20 and 21.
            showFunc();             // Call your function that shows the frequency 
        }
				//max_time++;
    } while (!isTuningComplete());

   reg0.raw = 0;
   reg0.refined.fm_en = this->currentMode;
   reg0.refined.mute = 0;     
   reg0.refined.power_on = 1; 
   reg0.refined.tune = 0;     
   reg0.refined.seek = 0;     
   reg0.refined.seekup = up_down;
   setRegister(REG00, reg0.raw);
   this->currentFrequency = channelToFrequency();

 }
void AKC695X::setFrequency(uint16_t frequency){
    uint16_t channel, tmpFreq;
    akc595x_reg2 reg2;
    akc595x_reg3 reg3;

    // Check the band limits
    if (frequency > this->currentBandMaximumFrequency)
        tmpFreq = this->currentBandMinimumFrequency;
    else if (frequency < this->currentBandMinimumFrequency )
        tmpFreq = this->currentBandMaximumFrequency;
    else
        tmpFreq = frequency; 

    reg2.raw = getRegister(REG02); // Gets the current value of the REG02

    if (this->currentMode == 0)
    {
        // AM mode
        channel = tmpFreq / this->currentStep;
        reg2.refined.channel = (channel >> 8);      // Changes just the 5 higher bits of the channel.
        reg2.refined.ref_32k_mode = this->currentCrystalType;
        reg2.refined.mode3k = this->currentMode3k;

        reg3 = channel & 0x0ff;               // Sets the 8 lower bits of the channel

        setRegister(REG03, reg3);
        setRegister(REG02, reg2.raw);
    }
    else
    {
        // FM mode
        channel = (tmpFreq - 300) * 4;
        reg2.refined.channel = (channel >> 8); 
        reg2.refined.ref_32k_mode = this->currentCrystalType;
        reg2.refined.mode3k = this->currentMode3k;
        reg3 = channel & 0x0ff;
        setRegister(REG03, reg3);
        setRegister(REG02, reg2.raw);
    }
    commitTune();
    this->currentFrequency = tmpFreq;
}

uint16_t AKC695X::getFrequency(){
    return this->currentFrequency;
}

void AKC695X::frequencyUp(){
    this->currentFrequency += this->currentStep;
    setFrequency(this->currentFrequency);
}

/**
 * @ingroup GA04
 * @brief Subtracts the current step from the current frequency and assign the new frequency
 * @details Goes to the previous frequency channel  
 */
void AKC695X::frequencyDown(){
    this->currentFrequency -= this->currentStep;
    setFrequency(this->currentFrequency);
}

void AKC695X::setAudio(uint8_t phase_inv, uint8_t line, uint8_t volume){
    akc595x_reg6 reg6;

    this->volume = reg6.refined.volume = (volume > 63) ? 63 : volume;
    reg6.refined.line = line;
    reg6.refined.phase_inv = phase_inv;
    setRegister(REG06, reg6.raw);
}
void AKC695X::setAudio(){
    setVolumeControl(1); // Audio controlled by MCU
    setAudio(0, 0, 40);
}

void AKC695X::setVolume(uint8_t volume){
    akc595x_reg6 reg6;

    reg6.raw = getRegister(REG06); // gets the current register value;

    if (volume > 63)
        volume = 63;
    this->volume = reg6.refined.volume = volume; // changes just the volume attribute
    setRegister(REG06, reg6.raw);                // writes the new reg9 value
}
void AKC695X::setVolumeUp(){
    this->volume = (this->volume > 63) ? 63 : (this->volume + 1);
    setVolume(this->volume);
}
void AKC695X::setVolumeDown(){
    this->volume = (this->volume < 25) ? 25 : (this->volume - 1);
    setVolume(this->volume);
}
void AKC695X::setVolumeControl(uint8_t type){
    akc595x_reg9 reg9;

    reg9.raw = getRegister(REG09);  // gets the current register value;
    reg9.refined.pd_adc_vol = type; // changes just the attribute pd_adc_vol
    setRegister(REG09, reg9.raw);   // writes the new reg9 value
}
int AKC695X::getRSSI(){
    akc595x_reg24 reg24;
    akc595x_reg27 reg27;

    int factor = (this->currentMode == CURRENT_MODE_FM || this->currentFrequency > 3000) ? 103 : 123;

    reg24.raw = getRegister(REG24);
    reg27.raw = getRegister(REG27);

    // Calculates the RSSI
    return ( factor - reg27.refined.rssi - 6 * (reg24.refined.pgalevel_rf + reg24.refined.pgalevel_if) );
}
float AKC695X::getSupplyVoltage(){
    akc595x_reg25 reg25;
    reg25.raw = getRegister(REG25);
    return (1.8 + 0.05 * reg25.refined.vbat);
}
void AKC695X::setbass(uint8_t bass)
{
    akc595x_reg7 reg7;
    reg7.raw = getRegister(REG07); // Gets the current value
    reg7.refined.bben = bass;       // Sets just the attribute
    setRegister(REG07, reg7.raw);
}
