# AKC695X-STM32

#### 介绍
基于[pu2clr/AKC695X](https://github.com/pu2clr/AKC695X "github项目")的arduino项目进行的STM32移植

感谢pu2clr以及为该项目做出贡献的人

#### 软件架构
由CubeMX生成基本架构，我个人移植了AKC695X的库文件，提供一份基于串口通信的示例文件，接口定义详见**ioc文件**


#### 使用说明
函数声明和结构体已在AKC695X.h中列出，可以参考原作者工程使用

#### 结果
![组装图](.\photos\组装图.jpg)
![整体图](.\photos\整体图.jpg)
![运行界面](.\photos\运行界面.png)

请clone本工程后查看完整图片

#### 注意
1. 硬件中功率电源和信号电源均为3.3V，可用5V电源但不推荐。
2. STM32F403VET6开发板无法供足功率电，需要额外电源。
3. 控制dsp的reset引脚为PB5，需要接上。


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
